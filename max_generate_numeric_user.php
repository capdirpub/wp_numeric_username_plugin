<?php
/**
* Plugin Name: Max Generate Prefix Numeric User
* Plugin URI: https://maxenius.com/
* Author: Maxenius
* Author URI: http://maxenius.com/
* Text Domain: max-generate-prefix-numeric-user
* Domain Path: /languages/
* Description: This plugin for add username prefix.
* Version: 1.0
* License: GPL2
* License URI: https://www.gnu.org/license/gpl-2.0.html
**/

require plugin_dir_path(__FILE__) . 'plugin-update-checker/plugin-update-checker.php';

 $myUpdateChecker = new PluginUpdateChecker_3_1(
     'http://wp.capenq.com/?action=get_metadata&slug=wp_numeric_username_plugin',
     __FILE__, //Full path to the main plugin file or functions.php.
     'wp_numeric_username_plugin'
 );

 
register_activation_hook( __FILE__, 'username_prefix_act' );
function username_prefix_act()
{
    global $wpdb;

    $tablename = $wpdb->prefix . "users";
	
	$tablemeta = $wpdb->prefix . "usermeta";

    $last_user_name = $wpdb->get_var( "SELECT user_login FROM ".$tablename." u INNER JOIN ".$tablemeta." m ON m.user_id = u.ID WHERE m.meta_key = 'wp_capabilities' AND m.meta_value LIKE '%customer%' AND user_login REGEXP '^[0-9]+$' ORDER BY CONVERT(user_login, UNSIGNED) DESC, id DESC LIMIT 1");

    if(!empty($last_user_name)){

        update_option('custom_username',$last_user_name);
    }else{
        update_option('custom_username','12345');
    }
    
}
register_deactivation_hook( __FILE__, 'username_prefix_deact' );
function username_prefix_deact()
{
    delete_option('custom_username');
    delete_option('custom_username_set');
}

add_action("admin_menu", "add_username_menu");
function add_username_menu() {
  add_submenu_page(
        'users.php',
        'Set User Prefix',
        'Set User Prefix',
        'administrator',
        'set-user-prefix',
        'add_prefix_page' );
}
function add_prefix_page(){
    $cust_username_opt_val = get_option('custom_username');

    echo '
    <div style="padding:30px">
        <h2>Create Username</h2>
        <br>
        <form method="post" action="">
        Set Prefix : <input type="number" name="username" required/>
        <input type="submit"  class="button" name="add_username_form_submit" value="Create Prefix" /> 
        </form>
        <br>
        <b>Username Prefix Number:<b> ' . $cust_username_opt_val .'
    </div>
    ';
    if(isset($_SESSION['series_num']) && isset($_SESSION['error']) && $_SESSION['error'] == 1){
        $last_digit = $_SESSION['series_num'] + 1;
        echo '<div class="error notice">
            <p>Given username prefix '.$_POST['username'].' will casuse overlapping. Please start with '.$last_digit.'.</p>
        </div>';
    }
    else if(isset($_SESSION['error']) && $_SESSION['error'] == 2){
        echo '
            <div class="error notice">
                <p>Please Enter Prefix value</p>
            </div>
            ';
    }
    else if(isset($_SESSION['error']) && $_SESSION['error'] == 0){
        echo '
            <div class="success notice">
                <p>Successfully Update</p>
            </div>
            ';
    }
}
add_action( 'init', 'process_post' );
 
function process_post() {

    global $wpdb;

    $tablename = $wpdb->prefix . "users";
	
	$tablemeta = $wpdb->prefix . "usermeta";

    if( isset( $_POST['add_username_form_submit'] ) ) {

        if(!empty($_POST['username'])){

            $last_user_name = $wpdb->get_var( "SELECT user_login FROM ".$tablename." u INNER JOIN ".$tablemeta." m ON m.user_id = u.ID WHERE m.meta_key = 'wp_capabilities' AND m.meta_value LIKE '%customer%' AND user_login REGEXP '^[0-9]+$' ORDER BY CONVERT(user_login, UNSIGNED) DESC, id DESC LIMIT 1");

            $cust_username_new_val = (int) $_POST['username'];
            if(!empty($last_user_name)){
                $last_user_name = (int) $last_user_name;

                if($cust_username_new_val <= $last_user_name){

                    $_SESSION['error'] = 1;
                    $_SESSION['series_num'] = $last_user_name;

                }else{
                    update_option('custom_username',$cust_username_new_val);
                    delete_option('custom_username_set');
                    $_SESSION['error'] = 0;
                }
            }else{
                update_option('custom_username',$cust_username_new_val);
                delete_option('custom_username_set');
                $_SESSION['error'] = 0;
            }

        }else{
            $_SESSION['error'] = 2;

        }
    }
}
function add_custom_username($user_id){
    global $wpdb;

    $tablename = $wpdb->prefix . "users";
	$tablename2 = $wpdb->prefix . "wc_customer_lookup";

    $cust_username_opt_val = get_option('custom_username');

    $user_meta=get_userdata($user_id);

    $user_roles=$user_meta->roles;

    if(!in_array('customer',$user_roles)){
        return;
    }
	
    $last_user_name = $wpdb->get_var( "SELECT user_login FROM ".$tablename." u INNER JOIN wp_usermeta m ON m.user_id = u.ID WHERE m.meta_key = 'wp_capabilities' AND m.meta_value LIKE '%customer%' ORDER BY id DESC LIMIT 1,1");

    if($cust_username_opt_val == $last_user_name){

        $user_login = $last_user_name + 1;

        $wpdb->update( $tablename, array( 'user_login' => $user_login,'user_nicename' => $user_login ), array( 'ID' => $user_id ) );
		wp_update_user( array( 'ID' => $user_id, 'user_login' => $user_login ) );
		update_user_meta($user_id,'nickname',$user_login);
        update_option('custom_username_set',$user_login);
    }
    else{
        $last_set_num  = get_option('custom_username_set');
        if(!empty($last_set_num)){
            $user_login = $last_set_num + 1;

            $wpdb->update( $tablename, array( 'user_login' => $user_login,'user_nicename' => $user_login ), array( 'ID' => $user_id ) );
			wp_update_user( array( 'ID' => $user_id, 'user_login' => $user_login ) );
			update_user_meta($user_id,'nickname',$user_login);
            update_option('custom_username_set',$user_login);
        }else{
            $user_login = $cust_username_opt_val;

            $wpdb->update( $tablename, array( 'user_login' => $user_login,'user_nicename' => $user_login ), array( 'ID' => $user_id ) );
			wp_update_user( array( 'ID' => $user_id, 'user_login' => $user_login ) );
			update_user_meta($user_id,'nickname',$user_login);
        }

    }

}
add_action( 'user_register', 'add_custom_username', 10, 1000 );
